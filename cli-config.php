<?php

use Symfony\Component\Yaml\Parser;
use Doctrine\ORM\Tools\Setup;
use ArchitectureLogic\Service\AbstractResourceService;

require '/vendor/autoload.php';
if (!is_file(AbstractResourceService::CONFIG_FILE)) {
    throw new Exception('Configuration file "' . AbstractResourceService::CONFIG_FILE . '" not found error.');
}

$yaml = new Parser();
$data = $yaml->parse(file_get_contents(AbstractResourceService::CONFIG_FILE));

$connectionOptions = $data['connection'];

$path = $data['entities_path'];
$devMode = $data['dev'];

$config = Setup::createAnnotationMetadataConfiguration($path, $devMode, null, null, false);
$em = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);

$helpers = new Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
));
