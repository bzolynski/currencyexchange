Currency Exchange
=================

Overview:
---------

    Simple example of Architecture and Domain logic using Slim Framework v2.6, Doctrine ORM, Twig templates, Yaml Parser and console command from Symfony framework.

1. Installation:
----------------

    <code>composer install</code>

    Rename config/resource.yml.dist to config/resource.yml and set configuration parameters
    
    Then create a database and load fixtures (see next chapter)

2. Development usage guidelines:
--------------------------------

    To create a database using Doctrine CLI from a console, execute a command in main path:

    <code>vendor/bin/doctrine orm:schema-tool:create</code>


    To drop a database using Doctrine CLI from a console, execute a command in main path:

    <code>vendor/bin/doctrine orm:schema-tool:drop --force</code>


    To load data fixtures from a CLI, execute a command in main path:

    <code>php cli.php fixtures:load</code>

3. End points:
--------------

    <code>POST /exchange</code> - submit a JSON, it will be logged by RequestLog entity:

    <code>{"userId": "134256", "currencyTo": "GBP", "currencyFrom": "EUR", "amountSell": 1000, "amountBuy": 747.10, "rate": 0.7471, "timePlaced" : "24-JAN-15 10:27:44", "originatingCountry" : "FR"}</code>


    GET / - frontend analyze of data including most common "from" / "to" currencies, currencies pairs and originating countries from where requests were submitted


    Note: Allowed currencies (loaded as data fixtures in src/DomainLogic/DataFixtures/ORM/LoadCurrencyData.php): 'GBP', 'EUR', 'CHF', 'USD', 'CAD', 'PLN'
