<?php

use Symfony\Component\Console\Application;

require_once('vendor/autoload.php');

$console = new Application();

//Load Fixtures method
require_once('cli/load-fixtures.php');

$console->run();
