<?php

namespace DomainLogic\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use DomainLogic\Entity\Currency;

class LoadCurrencyData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $currenciesCodes = array(
            'GBP', 'EUR', 'CHF', 'USD', 'CAD', 'PLN'
        );

        foreach($currenciesCodes as $currencyCode) {
            $currency = new Currency();
            $currency->setCode($currencyCode);
            $manager->persist($currency);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 0;
    }
}
