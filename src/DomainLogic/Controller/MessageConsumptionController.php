<?php

namespace DomainLogic\Controller;

use Slim\Http\Response;
use ArchitectureLogic\Controller\DefaultController;
use ArchitectureLogic\Service\ResourceFactoryService;

use DomainLogic\Entity\Currency;
use DomainLogic\Entity\RequestLog;

class MessageConsumptionController extends DefaultController
{

    /**
     * Receives exchange message
     * Method: POST
     * Path: /exchange
     *
     * @return Response
     */
    public function receiveExchangeMessageAction()
    {
        $requestData = json_decode($this->app->request->getBody(), true);

        $expectedData = array(
            'userId', 'currencyFrom', 'currencyTo', 'amountSell', 'amountBuy', 'rate', 'timePlaced', 'originatingCountry'
        );

        $missingData = array();
        foreach($expectedData as $field) {
            if (!isset($requestData[$field]) || !$requestData[$field]) {
                $missingData[] = $field;
            }
        }

        //Check if some parameters are missing
        if (!empty($missingData)) {
            return $this->response(DefaultController::HTTP_BAD_REQUEST, 'Parameters missing', $requestData, $missingData);
        }

        //Check if different currencies set
        if ($requestData['currencyFrom'] === $requestData['currencyTo']) {
            return $this->response(DefaultController::HTTP_BAD_REQUEST, 'Value of currencyFrom cannot be that same as value of currencyTo parameter.');
        }

        //Check if amount sell is equal to amount buy for a given rate
        if ($requestData['amountSell'] * $requestData['rate'] != $requestData['amountBuy']) {
            return $this->response(DefaultController::HTTP_BAD_REQUEST, 'AmountBuy inconsistency.');
        }

        //Check if country code exists
        if ($requestData['originatingCountry']) {
            // ...code here...
        }

        $currencyResource = new ResourceFactoryService('DomainLogic\Entity\Currency');

        //Checks if currencyFrom exists in database
        $requestCodes = array($requestData['currencyFrom'], $requestData['currencyTo']);
        $validCodes = $currencyResource->findBy(array('code' => $requestCodes));

        if (count($validCodes) !== 2) {

            $invalidCodes = array();
            $validCodesTable = array();

            /** @var Currency $validCode */
            foreach($validCodes as $validCode) {
                $validCodesTable[] = $validCode->getCode();
            }
            foreach($requestCodes as $requestCode) {
                if (!in_array($requestCode, $validCodesTable)) {
                    $invalidCodes[] = $requestCode;
                }
            }

            return $this->response(DefaultController::HTTP_BAD_REQUEST, 'Currency code from a request is not supported.', null, $invalidCodes);
        }

        $requestLog = new RequestLog();

        //Probably it would be quicker just to save values instead of currency ids as 1:n relation
        $requestLog->setCurrencyFrom($requestData['currencyFrom']);
        $requestLog->setCurrencyTo($requestData['currencyTo']);
        $requestLog->setCountry($requestData['originatingCountry']);

        $entityManager = $this->getEntityManager();
        $entityManager->persist($requestLog);
        $entityManager->flush();

        return $this->response(DefaultController::HTTP_OK, 'Ok', $requestData);
    }

    /**
     * Displays analysis of data
     *
     * Method: GET
     * Path: /
     *
     */
    public function analyzeAction()
    {

        //Count amount of the most common currencyFrom
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(RequestLog.currencyFrom) AS amount, RequestLog.currencyFrom')->
            from('DomainLogic\Entity\RequestLog', 'RequestLog')
            ->groupBy('RequestLog.currencyFrom')
            ->orderBy('amount', 'DESC')
        ;

        $mostCommonFrom = $qb->getQuery()->getResult();


        //Count amount of the most common currencyTo
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(RequestLog.currencyTo) AS amount, RequestLog.currencyTo')->
        from('DomainLogic\Entity\RequestLog', 'RequestLog')
            ->groupBy('RequestLog.currencyTo')
            ->orderBy('amount', 'DESC')
        ;

        $mostCommonTo = $qb->getQuery()->getResult();


        //Count amount of the most common currency pairs
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(CONCAT(RequestLog.currencyFrom, \'-\', RequestLog.currencyTo)) AS amount, CONCAT(RequestLog.currencyFrom, \'-\', RequestLog.currencyTo) AS pair')->
        from('DomainLogic\Entity\RequestLog', 'RequestLog')
            ->groupBy('pair')
            ->orderBy('amount', 'DESC')
        ;

        $mostCommonPair = $qb->getQuery()->getResult();


        //Count amount of originating countries
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('count(RequestLog.country) AS amount, RequestLog.country')->
        from('DomainLogic\Entity\RequestLog', 'RequestLog')
            ->groupBy('RequestLog.country')
            ->orderBy('amount', 'DESC')
        ;

        $originatingCountries = $qb->getQuery()->getResult();


        //Count all elements
        $allElements = 0;
        foreach($mostCommonFrom as $element) {
            $allElements+=$element['amount'];
        }
        $this->render('home' . DIRECTORY_SEPARATOR . 'index.twig.php', array(
            'mostCommonFrom' => $mostCommonFrom,
            'mostCommonTo' => $mostCommonTo,
            'mostCommonPair' => $mostCommonPair,
            'originatingCountries' => $originatingCountries,
            'all' => $allElements
        ));
    }
}
