<?php

namespace DomainLogic\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="request_logs")
 */
class RequestLog
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=3, name="currency_from")
     */
    protected $currencyFrom;

    /**
     * @var string
     * @ORM\Column(type="string", length=3, name="currency_to")
     */
    protected $currencyTo;

    /**
     * @var string
     * @ORM\Column(type="string", length=48, name="country")
     */
    protected $country;

    public function getId() {
        return $this->id;
    }

    public function getCurrencyFrom() {
        return $this->currencyFrom;
    }

    public function setCurrencyFrom($currencyFrom) {
        $this->currencyFrom = $currencyFrom;
        return $this;
    }

    public function getCurrencyTo() {
        return $this->currencyTo;
    }

    public function setCurrencyTo($currencyTo) {
        $this->currencyTo = $currencyTo;
        return $this;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setCountry($country) {
        $this->country = $country;
        return $this;
    }
}
