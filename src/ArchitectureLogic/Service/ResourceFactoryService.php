<?php

namespace ArchitectureLogic\Service;

/**
 * Class ResourceFactoryService
 * @package ArchitectureLogic
 */
class ResourceFactoryService extends AbstractResourceService
{

    private $entityClass;

    public function __construct($entityClass)
    {
        $this->entityClass = $entityClass;
        return $this->getEntityManager()->getRepository($this->entityClass);
    }

    public function find($id) {
        return $this->getEntityManager()->find($this->entityClass, $id);
    }

    public function findAll() {
        return $this->getEntityManager()->getRepository($this->entityClass)->findAll();
    }

    public function findOneBy(array $criteria) {
        return $this->getEntityManager()->getRepository($this->entityClass)->findOneBy($criteria);
    }

    public function findBy(array $criteria) {
        return $this->getEntityManager()->getRepository($this->entityClass)->findBy($criteria);
    }

}
