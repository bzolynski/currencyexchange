<?php

namespace ArchitectureLogic\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Yaml\Parser;
use Doctrine\ORM\Tools\Setup;
use Exception;

/**
 * Class AbstractResourceService
 * @package ArchitectureLogic
 */
abstract class AbstractResourceService
{

    const CONFIG_FILE = 'config/resource.yml';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager = null;

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if ($this->entityManager === null) {
            $this->entityManager = $this->createEntityManager();
        }

        return $this->entityManager;
    }

    /**
     * @return EntityManager
     * @throws Exception
     */
    public function createEntityManager()
    {

        if (!is_file(self::CONFIG_FILE)) {
            throw new Exception('Configuration file "' . self::CONFIG_FILE . '" not found error.');
        }

        $yaml = new Parser();
        $data = $yaml->parse(file_get_contents(self::CONFIG_FILE));

        $connectionOptions = $data['connection'];

        $path = $data['entities_path'];
        $devMode = $data['dev'];

        $config = Setup::createAnnotationMetadataConfiguration($path, $devMode, null, null, false);

        return EntityManager::create($connectionOptions, $config);
    }
}
