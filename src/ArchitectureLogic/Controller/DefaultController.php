<?php

namespace ArchitectureLogic\Controller;

use SlimController\SlimController;
use Slim\Http\Response;
use ArchitectureLogic\Service\EntityManagerService;

class DefaultController extends SlimController
{

    const HTTP_OK = 200;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_NOT_FOUND = 404;

    public function response($messageCode, $message, $data = null, $errors = null)
    {

        /** @var Response $response */
        $response = $this->app->response();

        $response['Content-Type'] = 'application/json';
        $response->setStatus(200);

        $responseData = array(
            'messageCode' => $messageCode,
            'message' => $message
        );
        if (!is_null($data)) {
            $responseData['data'] = $data;
        }
        if (!is_null($errors)) {
            $responseData['errors'] = $errors;
        }
        $response->setBody(json_encode(
            $responseData
        ));

        return $response;
    }

    public function getEntityManager()
    {
        $entityManager = new EntityManagerService();
        return $entityManager->getEntityManager();
    }
}
