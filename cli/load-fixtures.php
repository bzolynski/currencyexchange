<?php

use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

use ArchitectureLogic\Service\AbstractResourceService;
use Symfony\Component\Yaml\Parser;
use Doctrine\ORM\Tools\Setup;

use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Input\InputInterface;

require_once('vendor/autoload.php');

//Execution of this command: php cli.php fixtures:load
$console
    ->register('fixtures:load')
    ->setDescription('Displays the files in the given directory')
    ->setCode(function (InputInterface $input, OutputInterface $output) {

        $output->writeln(sprintf('Purging database...'));

        $loader = new Loader();
        $loader->loadFromDirectory('src/');

        if (!is_file(AbstractResourceService::CONFIG_FILE)) {
            throw new Exception('Configuration file "' . AbstractResourceService::CONFIG_FILE . '" not found error.');
        }

        $yaml = new Parser();
        $data = $yaml->parse(file_get_contents(AbstractResourceService::CONFIG_FILE));

        $connectionOptions = $data['connection'];

        $path = $data['entities_path'];
        $devMode = $data['dev'];

        $config = Setup::createAnnotationMetadataConfiguration($path, $devMode, null, null, false);
        $em = \Doctrine\ORM\EntityManager::create($connectionOptions, $config);

        $helpers = new Symfony\Component\Console\Helper\HelperSet(array(
            'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($em->getConnection()),
            'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($em)
        ));

        $purger = new ORMPurger();
        $executor = new ORMExecutor($em, $purger);
        $executor->execute($loader->getFixtures());

        $output->writeln(sprintf('Data fixtures loaded...'));
    })
;
