<?php

use SlimController\Slim;

require 'vendor/autoload.php';

$app = new Slim(array(
    'templates.path'             => './templates',
    'controller.class_prefix'    => '\\DomainLogic\\Controller',
    'controller.method_suffix'   => 'Action',
    'controller.template_suffix' => 'php',
    'view' => new \Slim\Views\Twig()
));

require 'config/routing.php';

$app->run();
