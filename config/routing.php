<?php

$app->addRoutes(array(
    '/exchange' => array('post' => 'MessageConsumptionController:receiveExchangeMessage'),
    '/' => array('get' => 'MessageConsumptionController:analyze')
));
