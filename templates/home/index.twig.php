{% extends "home/base.html" %}

{% block title %}Currency Exchange Analysis{% endblock %}

{% block content %}

<h1>Most common "from" currency:</h1>

<div class="chart">
    {% for element in mostCommonFrom %}
    <div class="bar color{{ loop.index % 5 }}" style="height: {{ ( 500 * element.amount / all ) | round }}px;">
        <strong>{{ element.currencyFrom }}</strong><br>{{ ( 100 * element.amount / all ) | round }}%<br>({{ element.amount }})
    </div>
    {% endfor %}
</div>

<h1>Most common "to" currency:</h1>

<div class="chart">
    {% for element in mostCommonTo %}
    <div class="bar color{{ loop.index % 5}}" style="height: {{ ( 500 * element.amount / all )|round }}px;">
        <strong>{{ element.currencyTo }}</strong><br>{{ ( 100 * element.amount / all ) | round }}%<br>({{ element.amount }})
    </div>
    {% endfor %}
</div>

<h1>Most common "from-to" currency pairs:</h1>

<div class="chart">
    {% for element in mostCommonPair %}
    <div class="bar color{{ loop.index % 5 }}" style="height: {{ ( 500 * element.amount / all )|round }}px;">
        <strong>{{ element.pair }}</strong><br>{{ ( 100 * element.amount / all ) | round }}%<br>({{ element.amount }})
    </div>
    {% endfor %}
</div>

<h1>Most common originating country requests:</h1>

<div class="chart">
    {% for element in originatingCountries %}
    <div class="bar color{{ loop.index % 5 }}" style="height: {{ ( 500 * element.amount / all )|round }}px;">
        <strong>{{ element.country }}</strong><br>{{ ( 100 * element.amount / all ) | round }}%<br>({{ element.amount }})
    </div>
    {% endfor %}
</div>

{% endblock %}
